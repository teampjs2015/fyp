/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.model;

/**
 *
 * @author admin
 */
public class Developer extends Person {
    private String employmentDate;
    private String nationality;
 
    
    public Developer(String username, String password,String type, String trellokey, String trellotoken, String employmentDate, String nationality){
        super(username, password, type, trellokey, trellotoken);
        this.employmentDate = employmentDate;
        this.nationality = nationality;
    }
    
    public Developer(Person p, String employmentDate, String nationailty){
        super(p.getUsername(), p.getPassword(), p.getType(), p.getTrelloKey(), p.getToken());
        this.employmentDate = employmentDate;
        this.nationality = nationality;
    }

    public String getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(String employmentDate) {
        this.employmentDate = employmentDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    
    
}
